#!/bin/sh

if ! grep --quiet sup-mail debian/changelog; then
    echo "You are not in the top sup-mail package directory."
    exit 1
fi

git clone https://github.com/sup-heliotrope/sup.wiki.git doc/wiki
rake man
cp man/* debian/man/
mv debian/man/sup.1 debian/man/sup-mail.1

rm -rf man doc/wiki
